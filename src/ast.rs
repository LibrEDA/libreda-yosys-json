/*
 * Copyright (c) 2018-2021 Thomas Kramer.
 *
 * This file is part of LibrEDA 
 * (see https://codeberg.org/libreda).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! This module contains structs that describe the structure of a JSON netlist
//! as used by Yosys.

use std::collections::BTreeMap;
use libreda_db::netlist::direction::Direction;

use serde::{Deserialize, Serialize};

/// Represents a netlist generated by Yosys in JSON format.
#[derive(Debug, Serialize, Deserialize)]
pub struct YosysJsonNetlist {
    pub creator: Option<String>,
    pub modules: BTreeMap<String, Module>, // `BTreeMap` preserves the order of insertion.
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Module {
    pub ports: BTreeMap<String, Port>,
    pub cells: BTreeMap<String, Cell>,
    pub netnames: BTreeMap<String, NetName>,
}

/// Value for a constant signal: '0', '1', or 'z'.
/// 'z' means is used for floating nets that are not driven.
/// The value 'x' (don't-care) is not supported.
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum ConstantBit {
    /// Constant 0.
    #[serde(rename = "0")]
    Zero,
    /// Constant 1.
    #[serde(rename = "1")]
    One,
    /// Floating signal.
    #[serde(rename = "z")]
    HighImpedance,
    /// Don't care.
    #[serde(rename = "x")]
    DontCare,

}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(untagged)]
pub enum Bit {
    /// Unique identifier of the bit signal.
    Signal(usize),
    /// Constant value of the bit signal.
    Const(ConstantBit),
}

#[test]
fn test_read_bit() {
    assert_eq!(serde_json::from_str::<Bit>("123").unwrap(), Bit::Signal(123));
    assert_eq!(serde_json::from_str::<Bit>(r#""0""#).unwrap(), Bit::Const(ConstantBit::Zero));
    assert_eq!(serde_json::from_str::<Bit>(r#""1""#).unwrap(), Bit::Const(ConstantBit::One));
    assert_eq!(serde_json::from_str::<Bit>(r#""z""#).unwrap(), Bit::Const(ConstantBit::HighImpedance));
    assert_eq!(serde_json::from_str::<Bit>(r#""x""#).unwrap(), Bit::Const(ConstantBit::DontCare));
}

/// Direction of a input/output port.
#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub enum PortDirection {
    #[serde(rename = "input")]
    Input,
    #[serde(rename = "output")]
    Output,
    #[serde(rename = "inout")]
    InOut,
}

impl Into<Direction> for PortDirection {
    fn into(self) -> Direction {
        match self {
            PortDirection::Input => Direction::Input,
            PortDirection::Output => Direction::Output,
            PortDirection::InOut => Direction::InOut,
        }
    }
}

impl From<Direction> for PortDirection {
    fn from(direction: Direction) -> Self {
        match direction {
            Direction::Input => PortDirection::Input,
            Direction::Output => PortDirection::Output,
            Direction::InOut => PortDirection::InOut,
            _ => panic!("Unsupported conversion.")
        }
    }
}

/// Input/output port of a module.
#[derive(Debug, Serialize, Deserialize)]
pub struct Port {
    pub direction: PortDirection,
    /// Bit signals that are internally connected to the port.
    pub bits: Vec<Bit>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Cell {
    /// The module name instantiated here.
    #[serde(rename = "type")]
    pub cell_type: String,
    pub port_directions: Option<BTreeMap<String, PortDirection>>,
    /// For each port a list of nets to which it is connected.
    pub connections: BTreeMap<String, Vec<Bit>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NetName {
    /// Set to '1' if the netname is automatically created and likely not interesting to the user.
    pub hide_name: u8,
    /// Bit signals that are associated with this name.
    pub bits: Vec<Bit>,
}