/*
 * Copyright (c) 2018-2021 Thomas Kramer.
 *
 * This file is part of LibrEDA 
 * (see https://codeberg.org/libreda).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! Netlist reader for the Yosys JSON format.

use super::ast::*;
use log::debug;
use std::io::Read;

use libreda_db::netlist::prelude::*;
use std::collections::{HashMap, BTreeSet};
use libreda_db::netlist::traits::NetlistEdit;

/// Netlist reader for the Yosys JSON format.
#[derive(Default)]
pub struct YosysJsonNetlistReader {
    /// If set to true cells will be read but not populated with internal instances.
    load_blackboxes: bool,
    /// Don't use the names of the sub circuits as defined in the JSON netlist.
    drop_sub_circuit_names: bool,
    /// Replace names that are marked to be hidden.
    hide_hidden_names: bool,
}

impl YosysJsonNetlistReader {

    /// Create a default JSON netlist reader.
    pub fn new() -> Self {
        YosysJsonNetlistReader::default()
    }

    /// If set to true cells will be read but not populated with internal instances.
    /// This way the reader will read only cell definitions but does not care about
    /// the internals of the cell. This is useful for loading libraries of standard-cells
    /// where the content of the standard-cell is not needed.
    pub fn load_blackboxes(mut self, load_blackboxes: bool) -> Self {
        self.load_blackboxes = load_blackboxes;
        self
    }

    /// If set to true the names of the sub circuits will not be used.
    pub fn drop_sub_circuit_names(mut self, drop_sub_circuit_names: bool) -> Self {
        self.drop_sub_circuit_names = drop_sub_circuit_names;
        self
    }

    /// Replace names that are marked to be hidden.
    pub fn hide_hidden_names(mut self, hide_hidden_names: bool) -> Self {
        self.hide_hidden_names = hide_hidden_names;
        self
    }
}


impl NetlistReader for YosysJsonNetlistReader {
    type Error = std::io::Error;

    fn read_into_netlist<R: Read, N: NetlistEdit>(&self,
                                                  reader: &mut R,
                                                  netlist: &mut N,
    ) -> Result<(), std::io::Error> {
        // Parse the JSON data.
        debug!("Parse JSON.");
        let result: YosysJsonNetlist = serde_json::from_reader(reader)?;
        debug!("Parsing JSON done.");

        // Mapping from bit-net numbers to `Net`s.
        let mut nets = HashMap::new();

        // Get or create a net based on the 'bit' net number from the JSON netlist.
        let mut get_or_create_net = |netlist: &mut N,
                                     circuit_id: &N::CircuitId,
                                     bit: &Bit,
                                     name: Option<String>| -> Option<N::NetId> {
            let name: Option<N::NameType> = name.map(|n| n.into());
            match bit {
                Bit::Signal(bitnum) => {
                    let net = nets.entry((circuit_id.clone(), *bitnum))
                        .or_insert(netlist.create_net(circuit_id, name));
                    Some(net.clone())
                }
                Bit::Const(name) => {
                    match name {
                        ConstantBit::Zero => {
                            let n = netlist.net_zero(circuit_id);
                            netlist.rename_net(&circuit_id, &n, Some("__LOW__".to_string().into()));
                            Some(n)
                        }
                        ConstantBit::One => {
                            let n = netlist.net_one(circuit_id);
                            netlist.rename_net(&circuit_id, &n, Some("__HIGH__".to_string().into()));
                            Some(n)
                        }
                        ConstantBit::HighImpedance => None,
                        ConstantBit::DontCare => None,
                    }
                }
            }
        };

        // Mapping module name -> (port name -> bit index)
        let mut pin_ids_by_port: HashMap<String, HashMap<String, Vec<usize>>> = HashMap::new();

        // Create modules.
        debug!("Create module instances.");
        for (module_name, module) in &result.modules {
            debug!("Module '{:}'", module_name);

            // Create pins for the circuit.
            // The multi-pin ports are unrolled.
            let pins: Vec<(N::NameType, _)> = module.ports.iter()
                .flat_map(|(port_name, port)| {
                    let direction: Direction = port.direction.into();

                    port.bits.iter().enumerate().map(move |(i, _bit)| {
                        let pin_name = format!("{}_[{}]", port_name, i);
                        (pin_name.into(), direction)
                    })
                }).collect();

            // Create a fresh empty circuit.
            let circuit = netlist.create_circuit(module_name.clone().into(), pins);

            // Get the pin ids corresponding to the ports.
            // This is necessary because ports are unrolled into single pins.
            let _pin_ids_by_port: HashMap<String, Vec<usize>> = module.ports.iter()
                .map(|(port_name, port)| {
                    let pin_ids: Vec<_> = port.bits.iter()
                        .enumerate()
                        .map(move |(i, _bit)| i)
                        .collect();
                    (port_name.clone(), pin_ids)
                })
                .collect();
            pin_ids_by_port.insert(module_name.clone(), _pin_ids_by_port);

            // Connect pins to nets.
            let pins: Vec<_> = netlist.each_pin_vec(&circuit);
            // Find all nets and constants connected to the pins.
            let bits: Vec<Bit> = module.ports.iter()
                .flat_map(|(_port_name, port)| {
                    port.bits.iter().enumerate().map(move |(_i, bit)| {
                        bit.clone()
                    })
                }).collect();

            // Create lookup table to find net names of a bit line.
            // TODO: Use internment for strings.
            let netnames_by_bit =
                {
                    let mut netnames_by_bit: HashMap<usize, Vec<String>> = HashMap::new();

                    // Load net names when they are required.
                    if !self.drop_sub_circuit_names {
                        // Loop through all netnames and remember to which bits they are connected.
                        for (net_name, info) in &module.netnames {
                            for bit in &info.bits {
                                if !self.hide_hidden_names || info.hide_name == 0 {
                                    match bit {
                                        Bit::Signal(bit_num) => {
                                            // Check that there are no name conflicts.
                                            if let Some(names) = netnames_by_bit.get_mut(bit_num) {
                                                names.push(net_name.clone())
                                            } else {
                                                netnames_by_bit.insert(*bit_num, vec![net_name.clone()]);
                                            }
                                        }
                                        _ => unimplemented!()
                                    }
                                }
                            }
                        }
                    }
                    netnames_by_bit
                };

            // Connect pins to nets and assign netnames.
            for (pin, bit) in pins.iter().zip(bits.iter()) {
                // Find net name.
                let name = match bit {
                    Bit::Signal(n) => {
                        let names = netnames_by_bit.get(n);
                        names.and_then(|names| {
                            names.iter()
                                // Skip names that are used for port names.
                                .filter(|&n| !module.ports.contains_key(n))
                                .nth(0)
                                .cloned()
                        })
                    }
                    Bit::Const(c) => match c {
                        ConstantBit::Zero => Some("__LOW__".to_string()),
                        ConstantBit::One => Some("__HIGH__".to_string()),
                        ConstantBit::HighImpedance => None,
                        ConstantBit::DontCare => None, // How to handle don't care nets?
                    }
                };

                let net = get_or_create_net(netlist, &circuit, &bit, name);
                netlist.connect_pin(pin, net);
            }
        }

        // Create module instances (sub-circuits).
        if !self.load_blackboxes {
            // Keep track of circuits that could not be found while creating circuit instances.
            let mut missing_circuits = BTreeSet::new();

            // Create module instances (sub-circuits).
            for (module_name, module) in &result.modules {
                let circuit = netlist.circuit_by_name(module_name)
                    .expect("Circuit not found."); // This should actually not happen.
                // Create instances.
                for (i, (inst_name, cell)) in module.cells.iter().enumerate() {

                    // Possibly replace the instance name.
                    let inst_name = if self.drop_sub_circuit_names {
                        let new_inst_name = format!("_{}", i);
                        debug!("Rename instance: '{}' -> '{}'", inst_name, new_inst_name);
                        new_inst_name
                    } else {
                        inst_name.clone()
                    };

                    debug!("Create instance: '{}'", inst_name);

                    // Get the circuit template name of this instance.
                    let circuit_type = &cell.cell_type;
                    // Try to find the circuit template. It is possible that a circuit is referenced
                    // but not defined in this netlist.
                    if let Some(template_circuit) = netlist.circuit_by_name(circuit_type) {
                        // Create the instance.
                        let inst = netlist.create_circuit_instance(&circuit, &template_circuit,
                                                                   Some(inst_name.into()));

                        // Connect circuit instance pins.
                        // Get the bits that are assigned to the pins. Here they are all flattened.
                        // TODO: This is not yet correct. Make proper use of `ports`.
                        let flat_connections: Vec<_> = cell.connections.values()
                            .flat_map(|bits| bits.iter())
                            .collect();

                        // Get all pins.
                        let pin_instances = netlist.each_pin_instance_vec(&inst);

                        // Connect all pins to the corresponding nets.
                        for (pin_inst, bit) in pin_instances.iter().zip(flat_connections) {
                            // Find the right net for this bit.
                            let net = get_or_create_net(netlist, &circuit, bit, None);
                            // Connect the pin.
                            netlist.connect_pin_instance(pin_inst, net);
                        }

                        // for (port_name, connections) in &cell.connections {
                        //     // TODO: Use `ports` of the circuit as soon as implemented in DB.
                        //     // dbg!(&pin_ids_by_port);
                        //     // dbg!((circuit_type, port_name));
                        //     // let pin_ids = pin_ids_by_port.get(circuit_type)
                        //     //     .expect("No port information for this circuit template.")
                        //     //     .get(port_name)
                        //     //     .expect("No pin ids found for this port.");
                        //
                        //
                        //     // Connect pins with nets.
                        //     let pin_instances = inst.each_pin_instance_vec();
                        //     dbg!(&pin_instances.len());
                        //     dbg!(&connections);
                        //     for (pin_inst, bit) in pin_instances.iter().zip(connections) {
                        //         // Find the right net for this bit.
                        //         dbg!(pin_inst.pin().name(), &bit);
                        //         let net = get_or_create_net(&circuit, bit, None);
                        //         // Connect the pin.
                        //         pin_inst.connect_net(net.as_ref());
                        //     }
                        //     // for (&pin_id, bit) in pin_ids.iter().zip(connections) {
                        //     //     // Find the right net for this bit.
                        //     //     let net = get_or_create_net(&circuit, bit, None);
                        //     //     // Connect the pin.
                        //     //     inst.connect_pin_by_id(pin_id, net.as_ref());
                        //     // }
                        // }
                    } else {
                        // Remember that the circuit could not be found.
                        missing_circuits.insert(circuit_type.clone());
                    }
                }
            }

            // Check if any circuits could not be found while creating circuit instances.
            if !missing_circuits.is_empty() {
                panic!("Following circuits could not be found: {:?}", missing_circuits); // TODO: Proper error type.
            }
        } else {
            debug!("Loading blackboxes only. Cells don't get populated with instances.")
        }

        Ok(())
    }
}

