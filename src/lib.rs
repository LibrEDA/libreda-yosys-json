/*
 * Copyright (c) 2018-2021 Thomas Kramer.
 *
 * This file is part of LibrEDA 
 * (see https://codeberg.org/libreda).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! This crate provides serialization and deserialization of netlists into the JSON format used
//! by Yosys. Reader and writer implement the `NetlistReader` and `NetlistWriter` traits of the LibrEDA data base.
//!
//! # Examples
//!
//! ## Read a netlist
//!
//! A typical netlist uses standard-cells from a library, hence the standard-cells definitions
//! are not contained in the netlist itself but in an other file.
//!
//! In this example, the library netlist is read first such that all standard-cell definitions are loaded
//! Then the netlist with the actual circuit is loaded.
//!
//! ```
//! use std::fs::File;
//! use libreda_yosys_json::*;
//! use libreda_db::prelude::*;
//!
//! // Locations of the library and the netlist.
//! let library_path = "./tests/data/gscl45nm_yosys.json";
//! let file_path = "./tests/data/comb_chip_small.json";
//! let mut f_library = File::open(library_path).unwrap();
//! let mut f_netlist = File::open(file_path).unwrap();
//!
//! // Create an empty netlist that will be populated from files.
//! let mut netlist = RcNetlist::new();
//!
//! // Create a reader that reads only cell definitions but does not care about
//! // the internals of the cell.
//! let library_reader = YosysJsonNetlistReader::new()
//!         .load_blackboxes(true);
//!
//! // Read the library.
//! library_reader.read_into_netlist(&mut f_library, &mut netlist).expect("Error while reading library.");
//!
//! // Read the netlist with a default reader (does not load black-boxes but populates the cells with content).
//! let reader = YosysJsonNetlistReader::new();
//! reader.read_into_netlist(&mut f_netlist, &mut netlist).expect("Error while reading netlist.");
//! ```

#![deny(missing_docs)]

mod ast;
mod reader;
mod writer;

pub use libreda_db::prelude::NetlistReader;
pub use libreda_db::prelude::NetlistWriter;

pub use reader::YosysJsonNetlistReader;
pub use writer::YosysJsonNetlistWriter;

