/*
 * Copyright (c) 2018-2021 Thomas Kramer.
 *
 * This file is part of LibrEDA 
 * (see https://codeberg.org/libreda).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! Netlist writer for the Yosys JSON format.

use log::debug;
use std::io::Write;

use super::ast::*;

use libreda_db::netlist::prelude::NetlistWriter;
use libreda_db::netlist::traits::NetlistBase;
use std::collections::{HashMap, HashSet};

/// Netlist writer for the Yosys JSON format.
pub struct YosysJsonNetlistWriter {
    /// Enable pretty format of the output JSON.
    pretty: bool,
    /// Name to write into the 'creator' field of the netlist.
    creator: String,
    /// Set of cell names that will not be written to the output.
    exclude_cells: HashSet<String>,
}

impl YosysJsonNetlistWriter {
    /// Create a default writer.
    pub fn new() -> Self {
        Self::default()
    }

    /// Enable pretty JSON format.
    pub fn pretty_print(mut self) -> Self {
        self.pretty = true;
        self
    }

    /// Set the name of the creator program. This will be written
    /// into the 'creator' field of the netlist.
    pub fn creator_string(mut self, creator: String) -> Self {
        self.creator = creator;
        self
    }
}

impl Default for YosysJsonNetlistWriter {
    fn default() -> Self {
        Self {
            pretty: false,
            creator: "libreda-yosys-json".to_string(),
            exclude_cells: HashSet::new()
        }
    }
}

impl NetlistWriter for YosysJsonNetlistWriter {
    type Error = serde_json::error::Error;

    fn write_netlist<W: Write, N: NetlistBase>(&self, writer: &mut W, netlist: &N) -> Result<(), Self::Error> {

        // 1. Construct the abstract syntax tree.
        // 2. Write the tree to JSON using `serde_json`.

        let mut net_mapping: HashMap<N::NetId, Bit> = HashMap::new();
        let mut bit_id_counter = 0;
        // Create a mapping from nets to the net identifiers used in the JSON format.
        // This happens on the fly. A new `Bit` identifier is created on first encounter of a net.
        let mut get_or_create_bit = |parent: &N::CircuitId, net: &N::NetId| -> Bit {
            if let Some(bit) = net_mapping.get(net) {
                bit.clone()
            } else {
                let bit = if net == &netlist.net_zero(parent) {
                    // Net is tied to constant LOW.
                    Bit::Const(ConstantBit::Zero)
                } else if net == &netlist.net_one(parent) {
                    // Net is tied to constant HIGH.
                    Bit::Const(ConstantBit::One)
                } else {
                    // Create a new ID for this signal.
                    let id = bit_id_counter;
                    bit_id_counter += 1;

                    Bit::Signal(id)
                };

                net_mapping.insert(net.clone(), bit.clone());

                bit
            }
        };

        let modules = netlist.each_circuit()
            // Get the circuit name.
            .map(|circuit_id| {
                let name: String = netlist.circuit_name(&circuit_id).into();
                (circuit_id, name)
            })
            .filter(|(_, name)| {
                let exclude = !self.exclude_cells.contains(name);
                if exclude {
                    debug!("Exclude cell '{}'.", name)
                }
                exclude
            })
            .map(|(circuit_id, circuit_name)| {

                // Get all pins of the circuit by name.
                let ports = netlist.each_pin(&circuit_id)
                    .map(|pin_id| {
                        let pin_name: String = netlist.pin_name(&pin_id).into();

                        let direction = netlist.pin_direction(&pin_id).into();

                        // Find the net connected to this pin.
                        let bits = netlist.net_of_pin(&pin_id)
                            .iter()
                            .map(|net| get_or_create_bit(&circuit_id, &net))
                            .collect();

                        let port = Port {
                            direction,
                            bits,
                        };

                        (pin_name, port)
                    })
                    .collect();

                // Get all instances inside this circuit by name.
                let cells = netlist.each_instance(&circuit_id)
                    .map(|inst_id| {
                        // Get name of the instance.
                        let inst_name: String = netlist.circuit_instance_name(&inst_id)
                            // TODO: Create name on the fly if none is defined.
                            .expect("Circuit instance must have a name.")
                            .into();

                        // Get the template circuit of the instance.
                        let template_circuit = netlist.template_circuit(&inst_id);
                        // Get name of the template circuit.
                        let cell_type = netlist.circuit_name(&template_circuit).into();

                        // Get all connected nets for each pin name.
                        let connections = netlist.each_pin(&template_circuit)
                            .zip(netlist.each_pin_instance(&inst_id))
                            .map(|(pin_id, pin_inst_id)| {
                                let pin_name = netlist.pin_name(&pin_id).into();

                                // Get bit numbers connected to this pin.
                                // TODO: Support for multi-bit ports.
                                let bits = netlist.net_of_pin_instance(&pin_inst_id)
                                    .iter()
                                    .map(|net| get_or_create_bit(&circuit_id, &net))
                                    .collect();

                                (pin_name, bits)
                            })
                            .collect();

                        // Port directions are written only for cells where the interface is known.
                        // This should be the case here (?).
                        let write_port_directions = true;

                        let port_directions = if write_port_directions {
                            // Get (pin name, direction) pairs.
                            Some(
                                netlist.each_pin(&template_circuit)
                                    .map(|pin_id| {
                                        let pin_name = netlist.pin_name(&pin_id).into();
                                        let direction = netlist.pin_direction(&pin_id).into();

                                        (pin_name, direction)
                                    })
                                    .collect()
                            )
                        } else {
                            None
                        };

                        // Construct the cell.
                        let cell = Cell {
                            cell_type,
                            port_directions,
                            connections,
                        };

                        (inst_name, cell)
                    })
                    .collect();

                let netnames = netlist.each_internal_net(&circuit_id)
                    // Omit constant LOW and HIGH nets.
                    .filter(|net| net != &netlist.net_zero(&circuit_id) &&
                        net != &netlist.net_one(&circuit_id))
                    .map(|net| {
                        let bit = get_or_create_bit(&circuit_id, &net);

                        let (net_name, hide_name) = match netlist.net_name(&net) {
                            Some(name) => (name.into(), 0),
                            None => {
                                // Construct a name.
                                let name = match &bit {
                                    Bit::Signal(n) => format!("__{}", n),
                                    Bit::Const(_) => panic!("A net should not be mapped to a constant.")
                                };

                                // Hide the constructed name.j
                                (name, 1)
                            }
                        };

                        let bits = vec![bit];

                        let net_details = NetName {
                            hide_name,
                            bits,
                        };

                        (net_name, net_details)
                    })
                    .collect();

                // Assemble the module.
                let module = Module {
                    ports,
                    cells,
                    netnames,
                };

                (circuit_name.into(), module)
            })
            .collect();

        // Assemble the top level of the tree.
        let ast = YosysJsonNetlist {
            creator: Some(self.creator.clone()),
            modules,
        };

        // Serialize to JSON.
        if self.pretty {
            debug!("Serialize netlist to pretty JSON.");
            serde_json::to_writer_pretty(writer, &ast)
        } else {
            debug!("Serialize netlist to JSON.");
            serde_json::to_writer(writer, &ast)
        }
    }
}