/*
 * Copyright (c) 2018-2021 Thomas Kramer.
 *
 * This file is part of LibrEDA 
 * (see https://codeberg.org/libreda).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

use std::fs::File;
use libreda_yosys_json::*;
use libreda_db::prelude::RcNetlist;

#[test]
fn test_read_empty_netlist() {
    let data = r#"
        {
            "creator": "Yosys...",
            "modules": {}
        }
        "#;

    let reader = YosysJsonNetlistReader::new();
    let _netlist: RcNetlist = reader.read_netlist(&mut data.as_bytes())
        .unwrap();
}

#[test]
fn test_write_empty_netlist() {
    let netlist = RcNetlist::new();
    let mut buf = Vec::new();

    let writer = YosysJsonNetlistWriter::new().pretty_print();
    writer.write_netlist(&mut buf, &netlist).unwrap();
}

#[test]
fn test_read_comb_chip_small() {
    let mut netlist = RcNetlist::new();
    let library_reader = YosysJsonNetlistReader::new()
        .load_blackboxes(true);
    let library_path = "./tests/data/gscl45nm_yosys.json";
    let file_path = "./tests/data/comb_chip_small.json";
    let mut f_library = File::open(library_path).unwrap();
    let mut f_netlist = File::open(file_path).unwrap();

    // Read library.
    library_reader.read_into_netlist(&mut f_library, &mut netlist)
        .unwrap();

    // Read netlist.
    let reader = YosysJsonNetlistReader::new();
    reader.read_into_netlist(&mut f_netlist, &mut netlist)
        .unwrap();

    // for c in netlist.each_circuit() {
    //     for child in c.each_child_recursive() {
    //         // dbg!(child.name());
    //     }
    // }
    // println!("{}", &netlist)
}


#[test]
fn test_read_write_comb_chip_small() {
    let mut netlist = RcNetlist::new();
    let library_reader = YosysJsonNetlistReader::new()
        .load_blackboxes(true);
    let library_path = "./tests/data/gscl45nm_yosys.json";
    let file_path = "./tests/data/comb_chip_small.json";
    let mut f_library = File::open(library_path).unwrap();
    let mut f_netlist = File::open(file_path).unwrap();

    // Read library.
    library_reader.read_into_netlist(&mut f_library, &mut netlist)
        .unwrap();

    // Read netlist.
    let reader = YosysJsonNetlistReader::new();
    reader.read_into_netlist(&mut f_netlist, &mut netlist)
        .unwrap();

    let writer = YosysJsonNetlistWriter::new()
        .pretty_print();
    let file_path = "./tests/data/comb_chip_small.out.json";
    let mut f_out = File::create(file_path).unwrap();
    writer.write_netlist(&mut f_out, &netlist).unwrap()
}

#[test]
fn test_seq_chip_simple() {
    let mut netlist = RcNetlist::new();
    let library_reader = YosysJsonNetlistReader::new()
        .load_blackboxes(true);
    let library_path = "./tests/data/gscl45nm_yosys.json";
    let file_path = "./tests/data/seq_chip_simple.json";
    let mut f_library = File::open(library_path).unwrap();
    let mut f_netlist = File::open(file_path).unwrap();

    // Read library.
    library_reader.read_into_netlist(&mut f_library, &mut netlist)
        .unwrap();

    // Read netlist.
    let reader = YosysJsonNetlistReader::new();
    reader.read_into_netlist(&mut f_netlist, &mut netlist)
        .unwrap();

    // dbg!(&netlist);
    println!("{}", &netlist)
}
